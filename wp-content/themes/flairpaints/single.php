<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="breadcrumb-box">
                <a href="http://flairpaints.loc/"><?php pll_e('homepage'); ?></a>
                <a href="<?php the_permalink($post->post_parent); ?>"> <?php pll_e('products'); ?></a>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>

            <div class="information-blocks">
                <div class="row">
                    <div class="information-entry article-container clearfix one-product">
                        <div class="col-lg-6">
                            <?php 
                                $image = get_the_post_thumbnail_url();
                            ?>
                            <a href="<?php echo $image; ?>">
                                <img src="<?php echo $image?>" alt="<?php echo the_title(); ?>" />
                            </a>
                            <div class="article-container style-1">
                            <?php
                                echo the_field('left_column');
                            ?>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h1 class="subpage-title">
                                <?php the_title(); ?>
                            </h1>
                            <?php dynamic_sidebar( 'social_media' ); ?>
                            <div class="article-container style-1">
                                <?php 
                                    while ( have_posts() ) : the_post();
                                        the_content();
                                    endwhile; // End of the loop.
                                ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <?php 
                            $descriptionValues = get_post_custom_values('description');
                            $applicationValues = get_post_custom_values('application');
                            
                            $description = $descriptionValues[0];
                            $application = $applicationValues[0];
                            $customInfos = get_post_custom();
                        ?>
                        <div class="information-blocks">
                            <div class="tabs-container style-1">
                                <div class="swiper-tabs tabs-switch">
                                    <div class="list">
                                    <?php foreach ($customInfos as $key => $value) : ?>
                                        <?php if(preg_match('/_/', $key) === 0) : ?>
                                            <a class="tab-switcher"><?php echo $key; ?></a>
                                        <?php endif; ?>
                                    <?php endforeach ?>
                                    <div class="clear"></div>
                                </div>
                            <div>
                            <?php foreach ($customInfos as $key => $value) : ?>
                                <?php if(preg_match('/_/', $key) === 0) : ?>
                                <div class="tabs-entry">
                                    <div class="article-container style-1">
                                        <div class="row">
                                            <?php echo $value[0]; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php endforeach ?>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
