<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<div class="content-push subpage">

    <div class="breadcrumb-box">
        <a href="http://flairpaints.loc/">Home</a>
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </div>

    <div class="information-blocks">
        <div class="row">
            <div class="information-entry article-container">
                <h1 class="subpage-title">
                    <?php the_title(); ?>
                </h1>
                <div class="article-container style-1">
                    <?php 
                        the_content();
                    ?>
                </div>
            </div>
        </div>
    </div>
    