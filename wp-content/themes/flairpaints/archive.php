<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">

		<div class="content-push">

                <?php 
                    if ($_POST) {
                        $data = $_POST;
                        
                        $options = explode('-', $data['sort']);
                        
                        $by_what = $options[0];
                        $direction = $options[1];
                    } else {
                        $by_what = 'order';
                        $direction = 'ASC';
                    }
                    $current = home_url($wp->request);
                    $currentExploded = explode('/', $current);
                    
                    if (end($currentExploded) == 'consigli-tecnici') {
                        $args = array(
                            'cat' => 3,
                        );
                    } else {
                        $args = array(
                            'cat' => get_query_var('cat'),
                            'post_type' => 'products',
                            'orderby' => $by_what,
                            'order' => $direction
                        );
                    }
                    $products = new WP_query($args);
                ?>
                <div class="upper-archive-part">
                    <div class="breadcrumb-box">
                        <a href="<?php echo home_url(); ?>"><?php pll_e('homepage'); ?></a>
                        <a href="<?php echo $current; ?>"><?php pll_e('products'); ?></a>
                    </div>
                    <div class="sorter">
                        <form action="<?php home_url(); ?>" method="post">
                            <select name="sort">
                                <option disabled selected value> -- <?php pll_e('sort'); ?> -- </option>
                                <option value="name-asc"><?php pll_e('name_asc'); ?></option>
                                <option value="name-desc"><?php pll_e('name_desc'); ?></option>
                                <option value="date-asc"><?php pll_e('date_asc'); ?></option>
                                <option value="date-desc"><?php pll_e('date_desc'); ?></option>
                            </select>  
                            <input type="submit" name="submit" value="<?php pll_e('sort_submit'); ?>" class="submit">
                        </form>
                    </div>
                </div>
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-md-12 information-entry">
                            <div class="blog-landing-box type-4">
                                <?php
                                // Start the Loop.
                                if ($products->have_posts()) {
                                        while ( $products->have_posts() ) : $products->the_post();

                                        $image = get_the_post_thumbnail_url();
                                        echo '
                                        <div class="blog-entry">
                                            <a class="image hover-class-1" href="'.get_permalink().'"><img src="' . $image . '" alt="" /><span class="hover-label">'.get_the_title().'</span></a>
                                            <div class="content">
                                                <a class="title" href="'.get_permalink().'">' . get_the_title() . '</a>
                                                <p class="excerpt">' . wp_trim_words(get_the_excerpt(), 30) . '</p>
                                            </div>
                                        </div>';

                                    // End the loop.
                                    endwhile;
                                } else {
                                    echo '<p class="text-center">'. pll_e('Sorry, no tips') .'</p>';
                                }

                                // Previous/next page navigation.
                                the_posts_pagination( array(
                                    'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                                    'next_text'          => __( 'Next page', 'twentyfifteen' ),
                                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                                ) );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

		</main><!-- #main -->

<?php get_footer();
