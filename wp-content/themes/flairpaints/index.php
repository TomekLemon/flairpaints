<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>

    <div id="content-block">

        <!-- HEADER -->
        <div class="header-wrapper style-5 style-7">
            <div class="mobile-nav">
                <a href="<?php home_url(); ?>"><img src="/wp-content/themes/flairpaints/assets/img/logo-7.png" alt="" /></a>
                <div id="nav-trigger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <nav style="display: none;">
                    <?php if ( has_nav_menu( 'top' ) ) : ?>
                        <div class="navigation-top">
                            <div class="wrap">
                                <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                            </div><!-- .wrap -->
                        </div><!-- .navigation-top -->
                    <?php endif; ?>
                </nav>
            </div>
            <header class="type-2">
                <div class="navigation-vertical-align">
                    <div class="cell-view logo-container">
                        <a id="logo" href="#"><img src="/wp-content/themes/flairpaints/assets/img/logo-7.png" alt="" /></a>
                    </div>
                    <div class="cell-view nav-container">
                        <div class="navigation">
                            <div class="navigation-header responsive-menu-toggle-class">
                                <div class="title">Navigation</div>
                                <div class="close-menu"></div>
                            </div>
                            <div class="nav-overflow">
                                <nav>
                                    <?php if ( has_nav_menu( 'top' ) ) : ?>
                                        <div class="navigation-top">
                                            <div class="wrap">
                                                <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                                            </div><!-- .wrap -->
                                        </div><!-- .navigation-top -->
                                    <?php endif; ?>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="close-header-layer"></div>
            </header>
            <div class="clear"></div>
        </div>

        <div class="content-push">

            <div class="parallax-slide">
                <div class="swiper-container" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide active" data-val="0" style="background-image: url(/wp-content/themes/flairpaints/assets/img/wide-1.jpg);"></div>
                        <div class="swiper-slide" data-val="1" style="background-image: url(/wp-content/themes/flairpaints/assets/img/wide-2.jpg);"></div>
                        <div class="swiper-slide" data-val="2" style="background-image: url(/wp-content/themes/flairpaints/assets/img/wide-3.jpg);"></div>
                    </div>
                    <div class="pagination"></div>
                </div>
            </div>
            <div class="row nopadding">
                <div class="col-sm-4 nopadding creative-square-box">
                    <div class="background-box" style="background-image: url(/wp-content/themes/flairpaints/assets/img/wide-4.jpg);"></div>
                    <div class="cell-view">
                        <div class="parallax-article">
                            <h1 class="title"><?php echo get_the_title(pll_get_post(5)); ?></h1>
                            <div class="info">
                                <a href="<?php echo get_permalink(pll_get_post(5)); ?>" class="button style-8"><?php pll_e('Read more')?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 nopadding creative-square-box">
                    <div class="background-box" style="background-image: url(/wp-content/themes/flairpaints/assets/img/wide-5.jpg);"></div>
                    <div class="cell-view">
                        <div class="parallax-article">
                            <h1 class="title"><?php echo get_cat_name(pll_get_term(3)); ?></h1>
                            <div class="info">
                                <a href="<?php echo get_category_link(pll_get_term(3)); ?>" class="button style-8"><?php pll_e('Read more')?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 nopadding creative-square-box">
                    <div class="background-box" style="background-image: url(/wp-content/themes/flairpaints/assets/img/wide-6.jpg);"></div>
                    <div class="cell-view">
                        <div class="parallax-article">
                            <h1 class="title"><?php echo get_the_title(pll_get_post(17)); ?></h1>
                            <div class="info">
                                <a href="<?php echo get_permalink(pll_get_post(17)); ?>" class="button style-8"><?php pll_e('Read more')?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wide-center">
                <div class="information-blocks">
                    <div class="block-header">
                        <h3 class="title"><?php pll_e('products'); ?></h3>
                    </div>
                    <div class="products-swiper">
                        <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="5">
                            <div class="swiper-wrapper">
                                <?php 
                                $args = array(
                                    'cat' => 2,
                                    'post_type' => 'products'
                                );
                                $products = new WP_Query($args);
                                
                                // Start the Loop.
                                while ( $products->have_posts() ) : $products->the_post();

                                    $image = get_the_post_thumbnail_url();
                                    echo '<div class="swiper-slide"> 
                                        <div class="paddings-container">
                                            <div class="product-slide-entry">
                                                <a class="product-image hover-class-1" href="' . get_post_permalink() . '">
                                                    <img src="' . $image . '" alt="" />
                                                    <span class="hover-label">' . pll__('Read more') .  '   </span>
                                                </a>
                                                <p class="title">'.get_the_title().'</p>
                                            </div>
                                        </div>
                                    </div>';

                                // End the loop.
                                endwhile;

                                // Previous/next page navigation.
                                the_posts_pagination( array(
                                    'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                                    'next_text'          => __( 'Next page', 'twentyfifteen' ),
                                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                                ) );
                                ?>
                            </div>
                            <div class="pagination"></div>
                        </div>
                    </div>
                </div>

                <div class="information-blocks">
                    <div class="block-header">
                        <h3 class="title"><?php pll_e('Technical tips'); ?></h3>
                    </div>
                    <div class="products-swiper">
                        <div class="swiper-container" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="2" data-int-slides="2" data-sm-slides="3" data-md-slides="4" data-lg-slides="5" data-add-slides="5">
                            <div class="swiper-wrapper">
                                <?php 
                                $args = array(
                                    'cat' => 3,
                                    'post_type' => 'tips'
                                );
                                $tips = new WP_query($args);
                                
                                if ($tips->have_posts()) {
                                    // Start the Loop.
                                    while ( $tips->have_posts() ) : $tips->the_post();

                                        $image = get_the_post_thumbnail_url();
                                        echo '<div class="swiper-slide"> 
                                        <div class="paddings-container">
                                            <div class="product-slide-entry tips-slide-entry">
                                                <a class="product-image hover-class-1" href="'.get_permalink($tip).'">
                                                    <img src="/wp-content/themes/flairpaints/assets/img/logo-7.png" alt="" />
                                                    <span class="hover-label">' . pll__('Read more') .  '</span>
                                                </a>
                                                <a class="subtitle" href="#">' . get_the_title() . '</a>
                                            </div>
                                        </div>
                                    </div>';

                                    // End the loop.
                                    endwhile;

                                    // Previous/next page navigation.
                                    the_posts_pagination( array(
                                        'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                                        'next_text'          => __( 'Next page', 'twentyfifteen' ),
                                        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                                    ) );
                                } else {
                                    echo '<p class="text-center">'. pll_e('Sorry, no tips') .'</p>';
                                }
                                ?>
                            </div>
                            <div class="pagination"></div>
                        </div>
                    </div>
                </div>

                <div class="clear"></div>
            </div>

            <!-- FOOTER -->
            <div class="footer-wrapper style-5 style-7">
                <footer class="type-2">
                    <div class="position-center">
                        <img class="footer-logo" src="/wp-content/themes/flairpaints/assets/img/logo-7.png" alt="" />
                            <?php if ( has_nav_menu( 'top' ) ) : ?>
                                <div class="navigation-top">
                                    <div class="wrap">
                                        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                                    </div><!-- .wrap -->
                                </div><!-- .navigation-top -->
                            <?php endif; ?>
                        <div class="copyright">Created with by <a href="#">8theme</a>. All right reserved</div>
                    </div>
                </footer>
            </div>
        </div>

    </div>

    <div class="search-box popup">
        <form>
            <div class="search-button">
                <i class="fa fa-search"></i>
                <input type="submit" />
            </div>
            <div class="search-drop-down">
                <div class="title"><span>All categories</span><i class="fa fa-angle-down"></i></div>
                <div class="list">
                    <div class="overflow">
                        <div class="category-entry">Category 1</div>
                        <div class="category-entry">Category 2</div>
                        <div class="category-entry">Category 2</div>
                        <div class="category-entry">Category 4</div>
                        <div class="category-entry">Category 5</div>
                        <div class="category-entry">Lorem</div>
                        <div class="category-entry">Ipsum</div>
                        <div class="category-entry">Dollor</div>
                        <div class="category-entry">Sit Amet</div>
                    </div>
                </div>
            </div>
            <div class="search-field">
                <input type="text" value="" placeholder="Search for product" />
            </div>
        </form>
    </div>

    <div class="cart-box popup">
        <div class="popup-container">
            <div class="cart-entry">
                <a class="image"><img src="/wp-content/themes/flairpaints/assets/img/product-menu-1.jpg" alt="" /></a>
                <div class="content">
                    <a class="title" href="#">Pullover Batwing Sleeve Zigzag</a>
                    <div class="quantity">Quantity: 4</div>
                    <div class="price">$990,00</div>
                </div>
                <div class="button-x"><i class="fa fa-close"></i></div>
            </div>
            <div class="cart-entry">
                <a class="image"><img src="/wp-content/themes/flairpaints/assets/img/product-menu-1_.jpg" alt="" /></a>
                <div class="content">
                    <a class="title" href="#">Pullover Batwing Sleeve Zigzag</a>
                    <div class="quantity">Quantity: 4</div>
                    <div class="price">$990,00</div>
                </div>
                <div class="button-x"><i class="fa fa-close"></i></div>
            </div>
            <div class="summary">
                <div class="subtotal">Subtotal: $990,00</div>
                <div class="grandtotal">Grand Total <span>$1029,79</span></div>
            </div>
            <div class="cart-buttons">
                <div class="column">
                    <a class="button style-3">view cart</a>
                    <div class="clear"></div>
                </div>
                <div class="column">
                    <a class="button style-4">checkout</a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div id="product-popup" class="overlay-popup">
        
        <div class="overflow">
            <div class="table-view">
                <div class="cell-view">
                    <div class="close-layer"></div>
                    <div class="popup-container">

                        <div class="row">
                            <div class="col-sm-6 information-entry">
                                <div class="product-preview-box">
                                    <div class="swiper-container product-preview-swiper" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide">
                                                <div class="product-zoom-image">
                                                    <img src="/wp-content/themes/flairpaints/assets/img/product-main-1.jpg" alt="" data-zoom="img/product-main-1-zoom.jpg" />
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="product-zoom-image">
                                                    <img src="/wp-content/themes/flairpaints/assets/img/product-main-2.jpg" alt="" data-zoom="img/product-main-2-zoom.jpg" />
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="product-zoom-image">
                                                    <img src="/wp-content/themes/flairpaints/assets/img/product-main-3.jpg" alt="" data-zoom="img/product-main-3-zoom.jpg" />
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="product-zoom-image">
                                                    <img src="/wp-content/themes/flairpaints/assets/img/product-main-4.jpg" alt="" data-zoom="img/product-main-4-zoom.jpg" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pagination"></div>
                                        <div class="product-zoom-container">
                                            <div class="move-box">
                                                <img class="default-image" src="/wp-content/themes/flairpaints/assets/img/product-main-1.jpg" alt="" />
                                                <img class="zoomed-image" src="/wp-content/themes/flairpaints/assets/img/product-main-1-zoom.jpg" alt="" />
                                            </div>
                                            <div class="zoom-area"></div>
                                        </div>
                                    </div>
                                    <div class="swiper-hidden-edges">
                                        <div class="swiper-container product-thumbnails-swiper" data-autoplay="0" data-loop="0" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="3" data-int-slides="3" data-sm-slides="3" data-md-slides="4" data-lg-slides="4" data-add-slides="4">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide selected">
                                                    <div class="paddings-container">
                                                        <img src="/wp-content/themes/flairpaints/assets/img/product-main-1.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="swiper-slide">
                                                    <div class="paddings-container">
                                                        <img src="/wp-content/themes/flairpaints/assets/img/product-main-2.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="swiper-slide">
                                                    <div class="paddings-container">
                                                        <img src="/wp-content/themes/flairpaints/assets/img/product-main-3.jpg" alt="" />
                                                    </div>
                                                </div>
                                                <div class="swiper-slide">
                                                    <div class="paddings-container">
                                                        <img src="/wp-content/themes/flairpaints/assets/img/product-main-4.jpg" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pagination"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 information-entry">
                                <div class="product-detail-box">
                                    <h1 class="product-title">T-shirt Basic Stampata</h1>
                                    <h3 class="product-subtitle">Loremous Clothing</h3>
                                    <div class="rating-box">
                                        <div class="star"><i class="fa fa-star"></i></div>
                                        <div class="star"><i class="fa fa-star"></i></div>
                                        <div class="star"><i class="fa fa-star"></i></div>
                                        <div class="star"><i class="fa fa-star-o"></i></div>
                                        <div class="star"><i class="fa fa-star-o"></i></div>
                                        <div class="rating-number">25 Reviews</div>
                                    </div>
                                    <div class="product-description detail-info-entry">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur.</div>
                                    <div class="price detail-info-entry">
                                        <div class="prev">$90,00</div>
                                        <div class="current">$70,00</div>
                                    </div>
                                    <div class="size-selector detail-info-entry">
                                        <div class="detail-info-entry-title">Size</div>
                                        <div class="entry active">xs</div>
                                        <div class="entry">s</div>
                                        <div class="entry">m</div>
                                        <div class="entry">l</div>
                                        <div class="entry">xl</div>
                                        <div class="spacer"></div>
                                    </div>
                                    <div class="color-selector detail-info-entry">
                                        <div class="detail-info-entry-title">Color</div>
                                        <div class="entry active" style="background-color: #d23118;">&nbsp;</div>
                                        <div class="entry" style="background-color: #2a84c9;">&nbsp;</div>
                                        <div class="entry" style="background-color: #000;">&nbsp;</div>
                                        <div class="entry" style="background-color: #d1d1d1;">&nbsp;</div>
                                        <div class="spacer"></div>
                                    </div>
                                    <div class="quantity-selector detail-info-entry">
                                        <div class="detail-info-entry-title">Quantity</div>
                                        <div class="entry number-minus">&nbsp;</div>
                                        <div class="entry number">10</div>
                                        <div class="entry number-plus">&nbsp;</div>
                                    </div>
                                    <div class="detail-info-entry">
                                        <a class="button style-10">Add to cart</a>
                                        <a class="button style-11"><i class="fa fa-heart"></i> Add to Wishlist</a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="tags-selector detail-info-entry">
                                        <div class="detail-info-entry-title">Tags:</div>
                                        <a href="#">bootstrap</a>/
                                        <a href="#">collections</a>/
                                        <a href="#">color/</a>
                                        <a href="#">responsive</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="close-popup"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#nav-trigger').on('click', function() {
            $('.mobile-nav > nav').toggleClass('show');
        });
    </script>

<?php get_footer();
