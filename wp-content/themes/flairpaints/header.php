<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<script src="/wp-content/themes/flairpaints/assets/js/jquery-2.1.3.min.js"></script>
<script src="/wp-content/themes/flairpaints/assets/js/idangerous.swiper.min.js"></script>
<script src="/wp-content/themes/flairpaints/assets/js/global.js"></script>
<script src="/wp-content/themes/flairpaints/assets/js/jquery.mousewheel.js"></script>
<script src="/wp-content/themes/flairpaints/assets/js/jquery.jscrollpane.min.js"></script>


<link href="/wp-content/themes/flairpaints/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/wp-content/themes/flairpaints/assets/css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
<link href="/wp-content/themes/flairpaints/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700%7CDancing+Script%7CMontserrat:400,700%7CMerriweather:400,300italic%7CLato:400,700,900' rel='stylesheet' type='text/css' />


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
<?php if(! is_front_page()): ?>
    <div class="content-center fixed-header-margin subpage-header">
    <!-- HEADER -->
    <div class="header-wrapper style-10">
        <div class="mobile-nav">
            <a href="<?php home_url(); ?>"><img src="/wp-content/themes/flairpaints/assets/img/logo-7.png" alt="" /></a>
            <div id="nav-trigger">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <nav>
                <?php if ( has_nav_menu( 'top' ) ) : ?>
                    <div class="navigation-top">
                        <div class="wrap">
                            <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                        </div><!-- .wrap -->
                    </div><!-- .navigation-top -->
                <?php endif; ?>
            </nav>
        </div>
        <header class="type-1">
            <div class="header-product">
                <div class="logo-wrapper">
                    <a id="logo" href="<?php echo home_url(); ?>"><img src="/wp-content/themes/flairpaints/assets/img/logo-7.png" alt="" /></a>
                </div>
            </div>

            <div class="close-header-layer"></div>
            <div class="navigation">
                <div class="navigation-header responsive-menu-toggle-class">
                    <div class="title">Navigation</div>
                    <div class="close-menu"></div>
                </div>
                <div class="nav-overflow">
                    <?php if ( has_nav_menu( 'top' ) ) : ?>
                        <div class="navigation-top">
                            <div class="wrap">
                                <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                            </div><!-- .wrap -->
                        </div><!-- .navigation-top -->
                    <?php endif; ?>
                    <div class="navigation-footer responsive-menu-toggle-class">
                        <div class="socials-box">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            <div class="clear"></div>
                        </div>
                        <div class="navigation-copyright">Created by <a href="#">8theme</a>. All rights reserved</div>
                    </div>
                </div>
            </div>
        </header>
        <div class="clear"></div>
    </div>
    
    <script>
        $('#nav-trigger').on('click', function() {
            $('.mobile-nav > nav').toggleClass('show');
        });
    </script>
<?php endif; ?>
