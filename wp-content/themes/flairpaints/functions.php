<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

add_theme_support('post-thumbnails');

/**
 * Register our sidebars and widgetized areas.
 *
 */
function social_widgets_init() {

	register_sidebar( array(
		'name'          => 'Social Media',
		'id'            => 'social_media',
		'before_widget' => '<a>',
		'after_widget'  => '</a>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );
}

function theme_styles()  
{ 
  // Register the style like this for a theme:  
  // (First the unique name for the style (custom-style) then the src, 
  // then dependencies and ver no. and media type)
  wp_enqueue_style( 'style-css', get_template_directory_uri() . '/style.css', array(), '8.0', 'all' );
}
add_action('wp_enqueue_scripts', 'theme_styles');

function choose_lang_init() {
    register_sidebar(array(
       'name' => 'Choose language', 
        'id' => 'choose_lang',
    ));
}
add_action( 'widgets_init', 'social_widgets_init' );
add_action( 'widgets_init', 'choose_lang_init' );

