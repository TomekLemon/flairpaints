<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'flairpaints');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`KL_5rbBnugC8#edhC/c0~Y:|u!iSW=*DwL|*JdUh[A{D3ES:/`8^f#^l|VBgj+~');
define('SECURE_AUTH_KEY',  '5;aB_`!=B1@p(pN]`GR%j;W1:hjQHGqN/k@{HyM#-)DP[7;KgH*=]J3`I-!^N0u6');
define('LOGGED_IN_KEY',    'K^=t$s|+2,J)q%>G8w]T4>4$>9Zr^8?wHbnp7EN*d^{IytD`#Eb@$7vE]``M4:&p');
define('NONCE_KEY',        'mK@9bwVYTIkPhMU}_;eI1Q)s}|9-):zlfjt4e^:?5yrrD#L!,v(GsN>wEmqqGU=8');
define('AUTH_SALT',        'gjgD.f4xW T/xMp-9pzVRVgHMBrTsxv%G>xF2XoEMpvJtOn$Fk14;V()t5M9wRR`');
define('SECURE_AUTH_SALT', '%iG]T`mdMLLC?Avop&n_fpnTr|{<Z%t(j|RPm@g{g_0tlVT+U)O#hxi;|.sp{4dT');
define('LOGGED_IN_SALT',   'ZvO|%@65CZrr$]PW;(UaqXzu#+z,vnU]F5R4QwVg}k>QM`g./7t)~P<wi~B+nx{Q');
define('NONCE_SALT',       '-%_ag$+2p@i=pA<d_Cfr};w4fjef<weXd&HT`piem&=i7m@xwXL<$~Lk):vOR!}u');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
